package projcet.test.student.tencennews;

import java.util.List;

/**
 * Created by Weli on 2018/7/2.
 */

public class Info {

    /**
     * rss : {"-version":"2.0","channel":{"title":"新闻国内","image":{"title":"新闻国内","link":"http://news.qq.com","url":"http://mat1.qq.com/news/rss/logo_news.gif"},"description":"新闻国内","link":"http://news.qq.com/china_index.shtml","copyright":"Copyright 1998 - 2005 TENCENT Inc. All Rights Reserved","language":"zh-cn","generator":"www.qq.com","item":[{"title":"收藏！一堂来自习近平总书记的党课","link":"http://news.qq.com/a/20180702/037596.htm","author":"www.qq.com","pubDate":"2018-07-02 19:07:50","description":"【\u201c学习笔记\u201d按】2017年10月25日，新当选的中央政治局常委会见中外记者，习近平总书记在讲话中指出：\u201c中国共产党是世界上最大的政党，大就要有大的样子。\u201d一句\u201c大就要有大的样子\u201d，引出了一个关于党建的大课题。作为党的\u201c细胞\u201d，党员的一言一行、一举一动都事关党的形象，如何做一名合格党员？今天，让我们一起来"},{"title":"胡海峰任浙江丽水市委书记","link":"http://news.qq.com/a/20180702/036495.htm","author":"www.qq.com","pubDate":"2018-07-02 18:22:14","description":"浙江省委决定：张兵同志任中共嘉兴市委书记，胡海峰同志任中共丽水市委书记。张兵简历张兵，男，汉族，1966年4月生，山东宁阳人，1985年4月加入中国共产党，1990年8月参加工作，研究生学历。曾任团浙江省委组织部副部长、常委、青农部部长、办公室主任、党组成员、副书记，衢州市副市长(挂职)，舟山市委常委、组织部部长、"},{"title":"8956.4万党员点亮8956.4万盏灯","link":"http://news.qq.com/a/20180702/034977.htm","author":"www.qq.com","pubDate":"2018-07-02 17:28:26","description":"中央组织部最新党内统计数据显示，截至2017年底，中国共产党党员总数为8956.4万名，比上年净增11.7万名。党的基层组织457.2万个，比上年增加5.3万个。数据表明，中央关于新形势下党员队伍建设和基层党组织建设的部署要求得到有效贯彻落实，党的生机与活力不断增强。(6月30日新华网)97年奋发图强，97载峥嵘岁月。从建党初的"},{"title":"贵州省梵净山被列入世界自然遗产名录","link":"http://news.qq.com/a/20180702/033301.htm","author":"www.qq.com","pubDate":"2018-07-02 16:44:45","description":"贵州省梵净山在世界遗产大会上获准列入世界自然遗产名录。至此，中国已拥有53处世界遗产。梵净山-红云金顶梵净山是武陵山脉的主峰，也是武陵山脉的标志。武陵山区自古相传的原生态巫、傩文化，是长江中、上游神秘诡谲文化的典范，其源流是由蚩尤为首的九黎传至三苗，并且融于南蛮。楚灭后蛮夷退守武陵，称为\u201c五溪蛮\u201d。由"},{"title":"陕西府谷致15死爆炸案公审：11被告人被控非法制造爆炸物","link":"http://news.qq.com/a/20180702/030011.htm","author":"www.qq.com","pubDate":"2018-07-02 15:23:34","description":"榆林市检察院指控，2016年10月24日14时许，袁保欣、陈英朝、郝小月等7人在制造过程中，搅拌原料时产生火花，引燃地面原料，浇水灭火未果后，七被告人逃离现场，随后发生爆炸，致府谷县新民镇周围民众15人死亡，16人重伤，38人轻伤，49人轻微伤，造成各项直接财产损失共计5926.32万元。"},{"title":"习近平把脉党内顽疾","link":"http://news.qq.com/a/20180702/029943.htm","author":"www.qq.com","pubDate":"2018-07-02 15:22:04","description":"形式主义、官僚主义危害极大。当前形式主义、官僚主义依然突出，又有新的表现形式。党的十八大以来，习近平总书记对形式主义、官僚主义等党内顽疾的实质、危害、表现形式进行了深入剖析，就如何反对形式主义、官僚主义有过多次重要指示。今天，党建网微平台与您一起了解总书记的部分重要论述！形式主义、官僚主义是我们党"},{"title":"美媒：AI助中国媒体提升全球影响力","link":"http://news.qq.com/a/20180702/028414.htm","author":"www.qq.com","pubDate":"2018-07-02 14:50:03","description":"资料图：蓝天白云映衬下的新华社新闻大厦。新华社发美国《哥伦比亚新闻评论》6月21日刊登文章称，中国领导人在2015年对宣传干部和新闻工作者的一次讲话中，阐述了他关于建立新的国际新闻秩序的设想：\u201c读者在哪里，受众在哪里，宣传报道的触角就要伸向哪里。\u201d随着中国最大的官方通讯社\u2014\u2014新华社启用\u201c媒体大脑\u201d，利用这"},{"title":"永葆政治本色 习近平这样为党庆生","link":"http://news.qq.com/a/20180702/022441.htm","author":"www.qq.com","pubDate":"2018-07-02 11:43:13","description":"央视网消息：党的政治建设是一个永恒课题。6月29日，中共中央政治局就加强党的政治建设举行第六次集体学习，习近平在主持学习时发表了重要讲话，以此庆祝党的97岁生日。此次政治局集体学习的目的是深化对党的政治建设的认识，增强推进党的政治建设的自觉性和坚定性。党的十九大明确提出党的政治建设这个重大命题，强调党的"},{"title":"南通毒贩马廷江脱逃事件涉事执勤法警被停职调查","link":"http://news.qq.com/a/20180702/019614.htm","author":"www.qq.com","pubDate":"2018-07-02 10:46:45","description":"今天上午，南通经济技术开发区人民法院发布关于被告人马廷江脱逃事件的情况通报，涉事执勤法警已于6月27日起停止履行职务，接受组织调查。通报全文如下：被告人马廷江脱逃事件发生后，本院积极配合公安机关进行追捕。涉事执勤法警已于6月27日起停止履行职务，接受组织调查。经初步核查，马廷江脱逃事件系本院司法安全保障"},{"title":"发改委：2020年底前，全国城市建立生活垃圾处理收费制度","link":"http://news.qq.com/a/20180702/019305.htm","author":"www.qq.com","pubDate":"2018-07-02 10:41:27","description":"国家发展改革委日前公布关于创新和完善促进绿色发展价格机制的意见。意见指出，要健全固体废物处理收费机制。2020年底前，全国城市及建制镇全面建立生活垃圾处理收费制度。同时，探索建立农村垃圾处理收费制度。相关意见原文如下：全面建立覆盖成本并合理盈利的固体废物处理收费机制，加快建立有利于促进垃圾分类和减量化"},{"title":"党的生日，国家大剧院用青藏铁路故事致敬铁道兵！","link":"http://news.qq.com/a/20180702/018861.htm","author":"www.qq.com","pubDate":"2018-07-02 10:33:59","description":"中国共产党建党97周年之际，青藏铁路也迎来了通车12周年纪念日。为纪念\u201c改革开放40周年\u201d暨青藏铁路建成通车12周年，国家大剧院原创舞剧《天路》于6月30日至7月3日迎来首轮演出。该剧以藏族同胞的视角，讲述了青藏铁路修建过程中，壮志凌云的铁道兵与藏族同胞间，从陌生戒备到心手相连的动人故事。《天路》宣传海报7月1日"},{"title":"中国人的故事|最美护士杜丽群：勇闯\u201c禁区\u201d，以爱抗艾","link":"http://news.qq.com/a/20180702/014850.htm","author":"www.qq.com","pubDate":"2018-07-02 09:33:17","description":"　　杜丽群在\u201c寻找最美医生\u201d颁奖典礼上。广西南宁市第四人民医院供图\u201c我想当妈妈了。\u201d当广西南宁第四人民医院艾滋病科护士长杜丽群听到患者小陈的心愿，心中有着隐隐的担心，已为人母的她非常理解这份心情，但对一个艾滋病患者而言，这不是一件易事。杜丽群耐心为她解释母婴阻断技术，悉心跟踪小陈的服药情况，解决小"},{"title":"红色基因是如何传承的？","link":"http://news.qq.com/a/20180702/014514.htm","author":"www.qq.com","pubDate":"2018-07-02 09:29:37","description":"【简介】\u201c心中有信仰，脚下有力量。\u201d支撑中国共产党人一路前行的，是内心深处对共产主义事业的伟大理想、对国家和民族的责任与担当。这就是共产党人的\u201c红色基因\u201d。共产党人是如何将\u201c红色基因\u201d代代相传的？让我们一起聆听一段故事\u2026\u2026　　记者：梁爱平、冯媛媛、王欢、曹力、白斌、杨焱彬、吴霞、狄春、宓盈婷、林凯"},{"title":"习近平心目中的优秀年轻干部是啥样？","link":"http://news.qq.com/a/20180702/013725.htm","author":"www.qq.com","pubDate":"2018-07-02 09:20:44","description":"导读近日，中央政治局会议审议《关于适应新时代要求大力发现培养选拔优秀年轻干部的意见》的消息，引发网友们的关注。关于什么是好干部，又该如何选贤任能、培养年轻干部，习近平论述过很多次。要敢于给他们压担子对那些看得准、有潜力、有发展前途的年轻干部，要敢于给他们压担子，有计划安排他们去经受锻炼。这种锻炼不"},{"title":"新华社评论员：抓好政治建设这个党的根本性建设","link":"http://news.qq.com/a/20180702/013241.htm","author":"www.qq.com","pubDate":"2018-07-02 09:16:28","description":"新华社北京7月1日电题：抓好政治建设这个党的根本性建设\u2014\u2014学习贯彻习近平总书记在中央政治局第六次集体学习重要讲话新华社评论员\u201c把党的政治建设作为党的根本性建设\u201d。党的97岁生日到来之际，中共中央政治局就加强党的政治建设举行第六次集体学习，习近平总书记主持学习并发表重要讲话。讲话深刻把握党的建设规律，深"},{"title":"云南师宗整治扶贫领域问题：有扶贫干部几乎不入户","link":"http://news.qq.com/a/20180702/002750.htm","author":"www.qq.com","pubDate":"2018-07-02 05:53:32","description":"看完通报批评的文件，云南省师宗县大同街道党工委委员、宣传委员李文齐犹豫了片刻，然后将文件的电子版转到了本镇扶贫干部的微信群。这次通报批评的对象不是别人，正是李文齐自己。前不久，李文齐等6人被师宗县纪委通报批评。\u201c绣花功夫扶贫，离不开一家一户上门。\u201d师宗县纪委监委工作人员告诉记者，有的地方扶贫干部驻村"},{"title":"人民日报谈中国与世贸组织白皮书：重诺笃行见大国担当","link":"http://news.qq.com/a/20180702/002580.htm","author":"www.qq.com","pubDate":"2018-07-02 05:24:44","description":"将改革进行到底，这也是中国履行承诺、践行自由贸易理念、遵守世贸规则、大幅开放市场、追求更广互利共赢的征程"},{"title":"霍启刚人民日报撰文：以香港所长应国家所需","link":"http://news.qq.com/a/20180702/002535.htm","author":"www.qq.com","pubDate":"2018-07-02 05:17:44","description":"香港青年的发展机遇源于国家整体发展，香港的未来植根于祖国的发展大局中。国家好，香港好，世界会更好"},{"title":"人民日报：始终对党忠诚老实 坚决反对做两面人","link":"http://news.qq.com/a/20180702/002414.htm","author":"www.qq.com","pubDate":"2018-07-02 05:01:19","description":"健全党和国家监督体系，强化自上而下的组织监督，改进自下而上的民主监督，发挥同级相互监督作用，加强对党员领导干部的日常管理监督，让两面人不能得利、无处遁形。"},{"title":"商务部长钟山人民日报撰文:开放的中国与世界共赢","link":"http://news.qq.com/a/20180702/002397.htm","author":"www.qq.com","pubDate":"2018-07-02 04:56:23","description":"今年是我国改革开放40周年，加入世贸组织已走过17个年头。习近平总书记指出：\u201c改革开放是中国和世界共同发展进步的伟大历程。\u201d加入世贸组织以来，我国与世界的关系日益密切、不断加深，成为改革开放的生动实践。站在新起点，在习近平新时代中国特色社会主义思想指引下，我国将坚定不移深化改革开放，支持完善多边贸易体"}]}}
     */

    @com.google.gson.annotations.SerializedName("rss")
    private RssBean rss;

    public RssBean getRss() {
        return rss;
    }

    public void setRss(RssBean rss) {
        this.rss = rss;
    }

    public static class RssBean {
        /**
         * -version : 2.0
         * channel : {"title":"新闻国内","image":{"title":"新闻国内","link":"http://news.qq.com","url":"http://mat1.qq.com/news/rss/logo_news.gif"},"description":"新闻国内","link":"http://news.qq.com/china_index.shtml","copyright":"Copyright 1998 - 2005 TENCENT Inc. All Rights Reserved","language":"zh-cn","generator":"www.qq.com","item":[{"title":"收藏！一堂来自习近平总书记的党课","link":"http://news.qq.com/a/20180702/037596.htm","author":"www.qq.com","pubDate":"2018-07-02 19:07:50","description":"【\u201c学习笔记\u201d按】2017年10月25日，新当选的中央政治局常委会见中外记者，习近平总书记在讲话中指出：\u201c中国共产党是世界上最大的政党，大就要有大的样子。\u201d一句\u201c大就要有大的样子\u201d，引出了一个关于党建的大课题。作为党的\u201c细胞\u201d，党员的一言一行、一举一动都事关党的形象，如何做一名合格党员？今天，让我们一起来"},{"title":"胡海峰任浙江丽水市委书记","link":"http://news.qq.com/a/20180702/036495.htm","author":"www.qq.com","pubDate":"2018-07-02 18:22:14","description":"浙江省委决定：张兵同志任中共嘉兴市委书记，胡海峰同志任中共丽水市委书记。张兵简历张兵，男，汉族，1966年4月生，山东宁阳人，1985年4月加入中国共产党，1990年8月参加工作，研究生学历。曾任团浙江省委组织部副部长、常委、青农部部长、办公室主任、党组成员、副书记，衢州市副市长(挂职)，舟山市委常委、组织部部长、"},{"title":"8956.4万党员点亮8956.4万盏灯","link":"http://news.qq.com/a/20180702/034977.htm","author":"www.qq.com","pubDate":"2018-07-02 17:28:26","description":"中央组织部最新党内统计数据显示，截至2017年底，中国共产党党员总数为8956.4万名，比上年净增11.7万名。党的基层组织457.2万个，比上年增加5.3万个。数据表明，中央关于新形势下党员队伍建设和基层党组织建设的部署要求得到有效贯彻落实，党的生机与活力不断增强。(6月30日新华网)97年奋发图强，97载峥嵘岁月。从建党初的"},{"title":"贵州省梵净山被列入世界自然遗产名录","link":"http://news.qq.com/a/20180702/033301.htm","author":"www.qq.com","pubDate":"2018-07-02 16:44:45","description":"贵州省梵净山在世界遗产大会上获准列入世界自然遗产名录。至此，中国已拥有53处世界遗产。梵净山-红云金顶梵净山是武陵山脉的主峰，也是武陵山脉的标志。武陵山区自古相传的原生态巫、傩文化，是长江中、上游神秘诡谲文化的典范，其源流是由蚩尤为首的九黎传至三苗，并且融于南蛮。楚灭后蛮夷退守武陵，称为\u201c五溪蛮\u201d。由"},{"title":"陕西府谷致15死爆炸案公审：11被告人被控非法制造爆炸物","link":"http://news.qq.com/a/20180702/030011.htm","author":"www.qq.com","pubDate":"2018-07-02 15:23:34","description":"榆林市检察院指控，2016年10月24日14时许，袁保欣、陈英朝、郝小月等7人在制造过程中，搅拌原料时产生火花，引燃地面原料，浇水灭火未果后，七被告人逃离现场，随后发生爆炸，致府谷县新民镇周围民众15人死亡，16人重伤，38人轻伤，49人轻微伤，造成各项直接财产损失共计5926.32万元。"},{"title":"习近平把脉党内顽疾","link":"http://news.qq.com/a/20180702/029943.htm","author":"www.qq.com","pubDate":"2018-07-02 15:22:04","description":"形式主义、官僚主义危害极大。当前形式主义、官僚主义依然突出，又有新的表现形式。党的十八大以来，习近平总书记对形式主义、官僚主义等党内顽疾的实质、危害、表现形式进行了深入剖析，就如何反对形式主义、官僚主义有过多次重要指示。今天，党建网微平台与您一起了解总书记的部分重要论述！形式主义、官僚主义是我们党"},{"title":"美媒：AI助中国媒体提升全球影响力","link":"http://news.qq.com/a/20180702/028414.htm","author":"www.qq.com","pubDate":"2018-07-02 14:50:03","description":"资料图：蓝天白云映衬下的新华社新闻大厦。新华社发美国《哥伦比亚新闻评论》6月21日刊登文章称，中国领导人在2015年对宣传干部和新闻工作者的一次讲话中，阐述了他关于建立新的国际新闻秩序的设想：\u201c读者在哪里，受众在哪里，宣传报道的触角就要伸向哪里。\u201d随着中国最大的官方通讯社\u2014\u2014新华社启用\u201c媒体大脑\u201d，利用这"},{"title":"永葆政治本色 习近平这样为党庆生","link":"http://news.qq.com/a/20180702/022441.htm","author":"www.qq.com","pubDate":"2018-07-02 11:43:13","description":"央视网消息：党的政治建设是一个永恒课题。6月29日，中共中央政治局就加强党的政治建设举行第六次集体学习，习近平在主持学习时发表了重要讲话，以此庆祝党的97岁生日。此次政治局集体学习的目的是深化对党的政治建设的认识，增强推进党的政治建设的自觉性和坚定性。党的十九大明确提出党的政治建设这个重大命题，强调党的"},{"title":"南通毒贩马廷江脱逃事件涉事执勤法警被停职调查","link":"http://news.qq.com/a/20180702/019614.htm","author":"www.qq.com","pubDate":"2018-07-02 10:46:45","description":"今天上午，南通经济技术开发区人民法院发布关于被告人马廷江脱逃事件的情况通报，涉事执勤法警已于6月27日起停止履行职务，接受组织调查。通报全文如下：被告人马廷江脱逃事件发生后，本院积极配合公安机关进行追捕。涉事执勤法警已于6月27日起停止履行职务，接受组织调查。经初步核查，马廷江脱逃事件系本院司法安全保障"},{"title":"发改委：2020年底前，全国城市建立生活垃圾处理收费制度","link":"http://news.qq.com/a/20180702/019305.htm","author":"www.qq.com","pubDate":"2018-07-02 10:41:27","description":"国家发展改革委日前公布关于创新和完善促进绿色发展价格机制的意见。意见指出，要健全固体废物处理收费机制。2020年底前，全国城市及建制镇全面建立生活垃圾处理收费制度。同时，探索建立农村垃圾处理收费制度。相关意见原文如下：全面建立覆盖成本并合理盈利的固体废物处理收费机制，加快建立有利于促进垃圾分类和减量化"},{"title":"党的生日，国家大剧院用青藏铁路故事致敬铁道兵！","link":"http://news.qq.com/a/20180702/018861.htm","author":"www.qq.com","pubDate":"2018-07-02 10:33:59","description":"中国共产党建党97周年之际，青藏铁路也迎来了通车12周年纪念日。为纪念\u201c改革开放40周年\u201d暨青藏铁路建成通车12周年，国家大剧院原创舞剧《天路》于6月30日至7月3日迎来首轮演出。该剧以藏族同胞的视角，讲述了青藏铁路修建过程中，壮志凌云的铁道兵与藏族同胞间，从陌生戒备到心手相连的动人故事。《天路》宣传海报7月1日"},{"title":"中国人的故事|最美护士杜丽群：勇闯\u201c禁区\u201d，以爱抗艾","link":"http://news.qq.com/a/20180702/014850.htm","author":"www.qq.com","pubDate":"2018-07-02 09:33:17","description":"　　杜丽群在\u201c寻找最美医生\u201d颁奖典礼上。广西南宁市第四人民医院供图\u201c我想当妈妈了。\u201d当广西南宁第四人民医院艾滋病科护士长杜丽群听到患者小陈的心愿，心中有着隐隐的担心，已为人母的她非常理解这份心情，但对一个艾滋病患者而言，这不是一件易事。杜丽群耐心为她解释母婴阻断技术，悉心跟踪小陈的服药情况，解决小"},{"title":"红色基因是如何传承的？","link":"http://news.qq.com/a/20180702/014514.htm","author":"www.qq.com","pubDate":"2018-07-02 09:29:37","description":"【简介】\u201c心中有信仰，脚下有力量。\u201d支撑中国共产党人一路前行的，是内心深处对共产主义事业的伟大理想、对国家和民族的责任与担当。这就是共产党人的\u201c红色基因\u201d。共产党人是如何将\u201c红色基因\u201d代代相传的？让我们一起聆听一段故事\u2026\u2026　　记者：梁爱平、冯媛媛、王欢、曹力、白斌、杨焱彬、吴霞、狄春、宓盈婷、林凯"},{"title":"习近平心目中的优秀年轻干部是啥样？","link":"http://news.qq.com/a/20180702/013725.htm","author":"www.qq.com","pubDate":"2018-07-02 09:20:44","description":"导读近日，中央政治局会议审议《关于适应新时代要求大力发现培养选拔优秀年轻干部的意见》的消息，引发网友们的关注。关于什么是好干部，又该如何选贤任能、培养年轻干部，习近平论述过很多次。要敢于给他们压担子对那些看得准、有潜力、有发展前途的年轻干部，要敢于给他们压担子，有计划安排他们去经受锻炼。这种锻炼不"},{"title":"新华社评论员：抓好政治建设这个党的根本性建设","link":"http://news.qq.com/a/20180702/013241.htm","author":"www.qq.com","pubDate":"2018-07-02 09:16:28","description":"新华社北京7月1日电题：抓好政治建设这个党的根本性建设\u2014\u2014学习贯彻习近平总书记在中央政治局第六次集体学习重要讲话新华社评论员\u201c把党的政治建设作为党的根本性建设\u201d。党的97岁生日到来之际，中共中央政治局就加强党的政治建设举行第六次集体学习，习近平总书记主持学习并发表重要讲话。讲话深刻把握党的建设规律，深"},{"title":"云南师宗整治扶贫领域问题：有扶贫干部几乎不入户","link":"http://news.qq.com/a/20180702/002750.htm","author":"www.qq.com","pubDate":"2018-07-02 05:53:32","description":"看完通报批评的文件，云南省师宗县大同街道党工委委员、宣传委员李文齐犹豫了片刻，然后将文件的电子版转到了本镇扶贫干部的微信群。这次通报批评的对象不是别人，正是李文齐自己。前不久，李文齐等6人被师宗县纪委通报批评。\u201c绣花功夫扶贫，离不开一家一户上门。\u201d师宗县纪委监委工作人员告诉记者，有的地方扶贫干部驻村"},{"title":"人民日报谈中国与世贸组织白皮书：重诺笃行见大国担当","link":"http://news.qq.com/a/20180702/002580.htm","author":"www.qq.com","pubDate":"2018-07-02 05:24:44","description":"将改革进行到底，这也是中国履行承诺、践行自由贸易理念、遵守世贸规则、大幅开放市场、追求更广互利共赢的征程"},{"title":"霍启刚人民日报撰文：以香港所长应国家所需","link":"http://news.qq.com/a/20180702/002535.htm","author":"www.qq.com","pubDate":"2018-07-02 05:17:44","description":"香港青年的发展机遇源于国家整体发展，香港的未来植根于祖国的发展大局中。国家好，香港好，世界会更好"},{"title":"人民日报：始终对党忠诚老实 坚决反对做两面人","link":"http://news.qq.com/a/20180702/002414.htm","author":"www.qq.com","pubDate":"2018-07-02 05:01:19","description":"健全党和国家监督体系，强化自上而下的组织监督，改进自下而上的民主监督，发挥同级相互监督作用，加强对党员领导干部的日常管理监督，让两面人不能得利、无处遁形。"},{"title":"商务部长钟山人民日报撰文:开放的中国与世界共赢","link":"http://news.qq.com/a/20180702/002397.htm","author":"www.qq.com","pubDate":"2018-07-02 04:56:23","description":"今年是我国改革开放40周年，加入世贸组织已走过17个年头。习近平总书记指出：\u201c改革开放是中国和世界共同发展进步的伟大历程。\u201d加入世贸组织以来，我国与世界的关系日益密切、不断加深，成为改革开放的生动实践。站在新起点，在习近平新时代中国特色社会主义思想指引下，我国将坚定不移深化改革开放，支持完善多边贸易体"}]}
         */

        @com.google.gson.annotations.SerializedName("-version")
        private String version;
        @com.google.gson.annotations.SerializedName("channel")
        private ChannelBean channel;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public ChannelBean getChannel() {
            return channel;
        }

        public void setChannel(ChannelBean channel) {
            this.channel = channel;
        }

        public static class ChannelBean {
            /**
             * title : 新闻国内
             * image : {"title":"新闻国内","link":"http://news.qq.com","url":"http://mat1.qq.com/news/rss/logo_news.gif"}
             * description : 新闻国内
             * link : http://news.qq.com/china_index.shtml
             * copyright : Copyright 1998 - 2005 TENCENT Inc. All Rights Reserved
             * language : zh-cn
             * generator : www.qq.com
             * item : [{"title":"收藏！一堂来自习近平总书记的党课","link":"http://news.qq.com/a/20180702/037596.htm","author":"www.qq.com","pubDate":"2018-07-02 19:07:50","description":"【\u201c学习笔记\u201d按】2017年10月25日，新当选的中央政治局常委会见中外记者，习近平总书记在讲话中指出：\u201c中国共产党是世界上最大的政党，大就要有大的样子。\u201d一句\u201c大就要有大的样子\u201d，引出了一个关于党建的大课题。作为党的\u201c细胞\u201d，党员的一言一行、一举一动都事关党的形象，如何做一名合格党员？今天，让我们一起来"},{"title":"胡海峰任浙江丽水市委书记","link":"http://news.qq.com/a/20180702/036495.htm","author":"www.qq.com","pubDate":"2018-07-02 18:22:14","description":"浙江省委决定：张兵同志任中共嘉兴市委书记，胡海峰同志任中共丽水市委书记。张兵简历张兵，男，汉族，1966年4月生，山东宁阳人，1985年4月加入中国共产党，1990年8月参加工作，研究生学历。曾任团浙江省委组织部副部长、常委、青农部部长、办公室主任、党组成员、副书记，衢州市副市长(挂职)，舟山市委常委、组织部部长、"},{"title":"8956.4万党员点亮8956.4万盏灯","link":"http://news.qq.com/a/20180702/034977.htm","author":"www.qq.com","pubDate":"2018-07-02 17:28:26","description":"中央组织部最新党内统计数据显示，截至2017年底，中国共产党党员总数为8956.4万名，比上年净增11.7万名。党的基层组织457.2万个，比上年增加5.3万个。数据表明，中央关于新形势下党员队伍建设和基层党组织建设的部署要求得到有效贯彻落实，党的生机与活力不断增强。(6月30日新华网)97年奋发图强，97载峥嵘岁月。从建党初的"},{"title":"贵州省梵净山被列入世界自然遗产名录","link":"http://news.qq.com/a/20180702/033301.htm","author":"www.qq.com","pubDate":"2018-07-02 16:44:45","description":"贵州省梵净山在世界遗产大会上获准列入世界自然遗产名录。至此，中国已拥有53处世界遗产。梵净山-红云金顶梵净山是武陵山脉的主峰，也是武陵山脉的标志。武陵山区自古相传的原生态巫、傩文化，是长江中、上游神秘诡谲文化的典范，其源流是由蚩尤为首的九黎传至三苗，并且融于南蛮。楚灭后蛮夷退守武陵，称为\u201c五溪蛮\u201d。由"},{"title":"陕西府谷致15死爆炸案公审：11被告人被控非法制造爆炸物","link":"http://news.qq.com/a/20180702/030011.htm","author":"www.qq.com","pubDate":"2018-07-02 15:23:34","description":"榆林市检察院指控，2016年10月24日14时许，袁保欣、陈英朝、郝小月等7人在制造过程中，搅拌原料时产生火花，引燃地面原料，浇水灭火未果后，七被告人逃离现场，随后发生爆炸，致府谷县新民镇周围民众15人死亡，16人重伤，38人轻伤，49人轻微伤，造成各项直接财产损失共计5926.32万元。"},{"title":"习近平把脉党内顽疾","link":"http://news.qq.com/a/20180702/029943.htm","author":"www.qq.com","pubDate":"2018-07-02 15:22:04","description":"形式主义、官僚主义危害极大。当前形式主义、官僚主义依然突出，又有新的表现形式。党的十八大以来，习近平总书记对形式主义、官僚主义等党内顽疾的实质、危害、表现形式进行了深入剖析，就如何反对形式主义、官僚主义有过多次重要指示。今天，党建网微平台与您一起了解总书记的部分重要论述！形式主义、官僚主义是我们党"},{"title":"美媒：AI助中国媒体提升全球影响力","link":"http://news.qq.com/a/20180702/028414.htm","author":"www.qq.com","pubDate":"2018-07-02 14:50:03","description":"资料图：蓝天白云映衬下的新华社新闻大厦。新华社发美国《哥伦比亚新闻评论》6月21日刊登文章称，中国领导人在2015年对宣传干部和新闻工作者的一次讲话中，阐述了他关于建立新的国际新闻秩序的设想：\u201c读者在哪里，受众在哪里，宣传报道的触角就要伸向哪里。\u201d随着中国最大的官方通讯社\u2014\u2014新华社启用\u201c媒体大脑\u201d，利用这"},{"title":"永葆政治本色 习近平这样为党庆生","link":"http://news.qq.com/a/20180702/022441.htm","author":"www.qq.com","pubDate":"2018-07-02 11:43:13","description":"央视网消息：党的政治建设是一个永恒课题。6月29日，中共中央政治局就加强党的政治建设举行第六次集体学习，习近平在主持学习时发表了重要讲话，以此庆祝党的97岁生日。此次政治局集体学习的目的是深化对党的政治建设的认识，增强推进党的政治建设的自觉性和坚定性。党的十九大明确提出党的政治建设这个重大命题，强调党的"},{"title":"南通毒贩马廷江脱逃事件涉事执勤法警被停职调查","link":"http://news.qq.com/a/20180702/019614.htm","author":"www.qq.com","pubDate":"2018-07-02 10:46:45","description":"今天上午，南通经济技术开发区人民法院发布关于被告人马廷江脱逃事件的情况通报，涉事执勤法警已于6月27日起停止履行职务，接受组织调查。通报全文如下：被告人马廷江脱逃事件发生后，本院积极配合公安机关进行追捕。涉事执勤法警已于6月27日起停止履行职务，接受组织调查。经初步核查，马廷江脱逃事件系本院司法安全保障"},{"title":"发改委：2020年底前，全国城市建立生活垃圾处理收费制度","link":"http://news.qq.com/a/20180702/019305.htm","author":"www.qq.com","pubDate":"2018-07-02 10:41:27","description":"国家发展改革委日前公布关于创新和完善促进绿色发展价格机制的意见。意见指出，要健全固体废物处理收费机制。2020年底前，全国城市及建制镇全面建立生活垃圾处理收费制度。同时，探索建立农村垃圾处理收费制度。相关意见原文如下：全面建立覆盖成本并合理盈利的固体废物处理收费机制，加快建立有利于促进垃圾分类和减量化"},{"title":"党的生日，国家大剧院用青藏铁路故事致敬铁道兵！","link":"http://news.qq.com/a/20180702/018861.htm","author":"www.qq.com","pubDate":"2018-07-02 10:33:59","description":"中国共产党建党97周年之际，青藏铁路也迎来了通车12周年纪念日。为纪念\u201c改革开放40周年\u201d暨青藏铁路建成通车12周年，国家大剧院原创舞剧《天路》于6月30日至7月3日迎来首轮演出。该剧以藏族同胞的视角，讲述了青藏铁路修建过程中，壮志凌云的铁道兵与藏族同胞间，从陌生戒备到心手相连的动人故事。《天路》宣传海报7月1日"},{"title":"中国人的故事|最美护士杜丽群：勇闯\u201c禁区\u201d，以爱抗艾","link":"http://news.qq.com/a/20180702/014850.htm","author":"www.qq.com","pubDate":"2018-07-02 09:33:17","description":"　　杜丽群在\u201c寻找最美医生\u201d颁奖典礼上。广西南宁市第四人民医院供图\u201c我想当妈妈了。\u201d当广西南宁第四人民医院艾滋病科护士长杜丽群听到患者小陈的心愿，心中有着隐隐的担心，已为人母的她非常理解这份心情，但对一个艾滋病患者而言，这不是一件易事。杜丽群耐心为她解释母婴阻断技术，悉心跟踪小陈的服药情况，解决小"},{"title":"红色基因是如何传承的？","link":"http://news.qq.com/a/20180702/014514.htm","author":"www.qq.com","pubDate":"2018-07-02 09:29:37","description":"【简介】\u201c心中有信仰，脚下有力量。\u201d支撑中国共产党人一路前行的，是内心深处对共产主义事业的伟大理想、对国家和民族的责任与担当。这就是共产党人的\u201c红色基因\u201d。共产党人是如何将\u201c红色基因\u201d代代相传的？让我们一起聆听一段故事\u2026\u2026　　记者：梁爱平、冯媛媛、王欢、曹力、白斌、杨焱彬、吴霞、狄春、宓盈婷、林凯"},{"title":"习近平心目中的优秀年轻干部是啥样？","link":"http://news.qq.com/a/20180702/013725.htm","author":"www.qq.com","pubDate":"2018-07-02 09:20:44","description":"导读近日，中央政治局会议审议《关于适应新时代要求大力发现培养选拔优秀年轻干部的意见》的消息，引发网友们的关注。关于什么是好干部，又该如何选贤任能、培养年轻干部，习近平论述过很多次。要敢于给他们压担子对那些看得准、有潜力、有发展前途的年轻干部，要敢于给他们压担子，有计划安排他们去经受锻炼。这种锻炼不"},{"title":"新华社评论员：抓好政治建设这个党的根本性建设","link":"http://news.qq.com/a/20180702/013241.htm","author":"www.qq.com","pubDate":"2018-07-02 09:16:28","description":"新华社北京7月1日电题：抓好政治建设这个党的根本性建设\u2014\u2014学习贯彻习近平总书记在中央政治局第六次集体学习重要讲话新华社评论员\u201c把党的政治建设作为党的根本性建设\u201d。党的97岁生日到来之际，中共中央政治局就加强党的政治建设举行第六次集体学习，习近平总书记主持学习并发表重要讲话。讲话深刻把握党的建设规律，深"},{"title":"云南师宗整治扶贫领域问题：有扶贫干部几乎不入户","link":"http://news.qq.com/a/20180702/002750.htm","author":"www.qq.com","pubDate":"2018-07-02 05:53:32","description":"看完通报批评的文件，云南省师宗县大同街道党工委委员、宣传委员李文齐犹豫了片刻，然后将文件的电子版转到了本镇扶贫干部的微信群。这次通报批评的对象不是别人，正是李文齐自己。前不久，李文齐等6人被师宗县纪委通报批评。\u201c绣花功夫扶贫，离不开一家一户上门。\u201d师宗县纪委监委工作人员告诉记者，有的地方扶贫干部驻村"},{"title":"人民日报谈中国与世贸组织白皮书：重诺笃行见大国担当","link":"http://news.qq.com/a/20180702/002580.htm","author":"www.qq.com","pubDate":"2018-07-02 05:24:44","description":"将改革进行到底，这也是中国履行承诺、践行自由贸易理念、遵守世贸规则、大幅开放市场、追求更广互利共赢的征程"},{"title":"霍启刚人民日报撰文：以香港所长应国家所需","link":"http://news.qq.com/a/20180702/002535.htm","author":"www.qq.com","pubDate":"2018-07-02 05:17:44","description":"香港青年的发展机遇源于国家整体发展，香港的未来植根于祖国的发展大局中。国家好，香港好，世界会更好"},{"title":"人民日报：始终对党忠诚老实 坚决反对做两面人","link":"http://news.qq.com/a/20180702/002414.htm","author":"www.qq.com","pubDate":"2018-07-02 05:01:19","description":"健全党和国家监督体系，强化自上而下的组织监督，改进自下而上的民主监督，发挥同级相互监督作用，加强对党员领导干部的日常管理监督，让两面人不能得利、无处遁形。"},{"title":"商务部长钟山人民日报撰文:开放的中国与世界共赢","link":"http://news.qq.com/a/20180702/002397.htm","author":"www.qq.com","pubDate":"2018-07-02 04:56:23","description":"今年是我国改革开放40周年，加入世贸组织已走过17个年头。习近平总书记指出：\u201c改革开放是中国和世界共同发展进步的伟大历程。\u201d加入世贸组织以来，我国与世界的关系日益密切、不断加深，成为改革开放的生动实践。站在新起点，在习近平新时代中国特色社会主义思想指引下，我国将坚定不移深化改革开放，支持完善多边贸易体"}]
             */

            @com.google.gson.annotations.SerializedName("title")
            private String title;
            @com.google.gson.annotations.SerializedName("image")
            private ImageBean image;
            @com.google.gson.annotations.SerializedName("description")
            private String description;
            @com.google.gson.annotations.SerializedName("link")
            private String link;
            @com.google.gson.annotations.SerializedName("copyright")
            private String copyright;
            @com.google.gson.annotations.SerializedName("language")
            private String language;
            @com.google.gson.annotations.SerializedName("generator")
            private String generator;
            @com.google.gson.annotations.SerializedName("item")
            private List<ItemBean> item;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public ImageBean getImage() {
                return image;
            }

            public void setImage(ImageBean image) {
                this.image = image;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getLink() {
                return link;
            }

            public void setLink(String link) {
                this.link = link;
            }

            public String getCopyright() {
                return copyright;
            }

            public void setCopyright(String copyright) {
                this.copyright = copyright;
            }

            public String getLanguage() {
                return language;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public String getGenerator() {
                return generator;
            }

            public void setGenerator(String generator) {
                this.generator = generator;
            }

            public List<ItemBean> getItem() {
                return item;
            }

            public void setItem(List<ItemBean> item) {
                this.item = item;
            }

            public static class ImageBean {
                /**
                 * title : 新闻国内
                 * link : http://news.qq.com
                 * url : http://mat1.qq.com/news/rss/logo_news.gif
                 */

                @com.google.gson.annotations.SerializedName("title")
                private String title;
                @com.google.gson.annotations.SerializedName("link")
                private String link;
                @com.google.gson.annotations.SerializedName("url")
                private String url;

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getLink() {
                    return link;
                }

                public void setLink(String link) {
                    this.link = link;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class ItemBean {
                /**
                 * title : 收藏！一堂来自习近平总书记的党课
                 * link : http://news.qq.com/a/20180702/037596.htm
                 * author : www.qq.com
                 * pubDate : 2018-07-02 19:07:50
                 * description : 【“学习笔记”按】2017年10月25日，新当选的中央政治局常委会见中外记者，习近平总书记在讲话中指出：“中国共产党是世界上最大的政党，大就要有大的样子。”一句“大就要有大的样子”，引出了一个关于党建的大课题。作为党的“细胞”，党员的一言一行、一举一动都事关党的形象，如何做一名合格党员？今天，让我们一起来
                 */

                @com.google.gson.annotations.SerializedName("title")
                private String title;
                @com.google.gson.annotations.SerializedName("link")
                private String link;
                @com.google.gson.annotations.SerializedName("author")
                private String author;
                @com.google.gson.annotations.SerializedName("pubDate")
                private String pubDate;
                @com.google.gson.annotations.SerializedName("description")
                private String description;

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getLink() {
                    return link;
                }

                public void setLink(String link) {
                    this.link = link;
                }

                public String getAuthor() {
                    return author;
                }

                public void setAuthor(String author) {
                    this.author = author;
                }

                public String getPubDate() {
                    return pubDate;
                }

                public void setPubDate(String pubDate) {
                    this.pubDate = pubDate;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }
            }
        }
    }
}
