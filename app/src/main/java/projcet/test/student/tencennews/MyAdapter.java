package projcet.test.student.tencennews;

/**
 * Created by Weli on 2018/6/26.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
/**
 * 自定义适配器
 *
 * @author qiangzi
 *
 */
public class MyAdapter extends BaseAdapter {


    private Context context = null;

    private LayoutInflater inflater = null;
    List<Info.RssBean.ChannelBean.ItemBean> list;
    public MyAdapter(List<  Info.RssBean.ChannelBean.ItemBean > list, Context context) {
        this.list = list;
        this.context = context;
        // 布局装载器对象
        inflater = LayoutInflater.from(context);
    }

    // 适配器中数据集中数据的个数
    @Override
    public int getCount() {
        return list.size();
    }

    // 获取数据集中与指定索引对应的数据项
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    // 获取指定行对应的ID
    @Override
    public long getItemId(int position) {
        return position;
    }

    // 获取每一个Item显示的内容
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_news, null);

            viewHolder.tv_detail = (TextView) convertView.findViewById(R.id.tv_detail);
            viewHolder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
            viewHolder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(viewHolder);// 通过setTag将ViewHolder和convertView绑定
        }  else {
            viewHolder = (ViewHolder) convertView.getTag(); // 获取，通过ViewHolder找到相应的控件
        }
        Info.RssBean.ChannelBean.ItemBean itemBean = list.get(position);
        viewHolder.tv_title.setText(itemBean.getTitle());
        viewHolder.tv_time.setText(itemBean.getPubDate());
        viewHolder.tv_detail.setText(itemBean.getDescription());
        return convertView;
    }

    /**
     * ViewHolder
     */
    class ViewHolder {

        TextView tv_detail;
        TextView tv_time ;
        TextView tv_title  ;
    }

}